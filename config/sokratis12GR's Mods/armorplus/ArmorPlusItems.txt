1. Coal Armor: armorplus:coal_helmet , armorplus:coal_chestplate , armorplus:coal_leggings , armorplus:coal_boots

2. Lapis Armor: armorplus:lapis_helmet , armorplus:lapis_chestplate , armorplus:lapis_leggings , armorplus:lapis_boots

3. Redstone Armor: armorplus:redstone_helmet , armorplus:redstone_chestplate , armorplus:redstone_leggings , armorplus:redstone_boots

4. Emerald Armor: armorplus:emerald_helmet , armorplus:emerald_chestplate , armorplus:emerald_leggings , armorplus:emerald_boots

5. Obsidian Armor: armorplus:obsidian_helmet , armorplus:obsidian_chestplate , armorplus:obsidian_leggings , armorplus:obsidian_boots

6. Lava Armor: armorplus:lava_helmet , armorplus:lava_chestplate , armorplus:lava_leggings , armorplus:lava_boots

7. Guardian Armor: armorplus:guardian_helmet , armorplus:guardian_chestplate , armorplus:guardian_leggings , armorplus:guardian_boots

8. Super Star Armor: armorplus:super_star_helmet , armorplus:super_star_chestplate , armorplus:super_star_leggings , armorplus:super_star_boots

9. Ender Dragon Armor: armorplus:ender_dragon_helmet , armorplus:ender_dragon_chestplate , armorplus:ender_dragon_leggings , armorplus:ender_dragon_boots

10. The Ultimate Armor: armorplus:the_ultimate_helmet , armorplus:the_ultimate_chestplate , armorplus:the_ultimate_leggings , armorplus:the_ultimate_boots

11. Reinforced Gold Armor: armorplus:reinforced_golden_helmet , armorplus:reinforced_golden_chestplate , armorplus:reinforced_golden_leggings , armorplus:reinforced_golden_boots

12. Reinforced Chain Armor: armorplus:reinforced_chain_helmet , armorplus:reinforced_chain_chestplate , armorplus:reinforced_chain_leggings , armorplus:reinforced_chain_boots

13. Reinforced Iron Armor: armorplus:reinforced_iron_helmet , armorplus:reinforced_iron_chestplate , armorplus:reinforced_iron_leggings , armorplus:reinforced_iron_boots

14. Reinforced Diamond Armor: armorplus:reinforced_diamond_helmet , armorplus:reinforced_diamond_chestplate , armorplus:reinforced_diamond_leggings , armorplus:reinforced_diamond_boots

15. Cobalt Armor: armorplus:cobalt_helmet , armorplus:cobalt_chestplate , armorplus:cobalt_leggings , armorplus:cobalt_boots

16. Ardite  Armor: armorplus:ardite_helmet , armorplus:ardite_chestplate , armorplus:ardite_leggings , armorplus:ardite_boots

17. Manyullyn Armor: armorplus:manyullyn_helmet , armorplus:manyullyn_lhestplate , armorplus:manyullyn_leggings , armorplus:manyullyn_boots

18. Pig Iron Armor: armorplus:pig_iron_helmet , armorplus:pig_iron_chestplate , armorplus:pig_iron_leggings , armorplus:pig_iron_boots

19. Knight Slime Armor: armorplus:knight_slime_helmet , armorplus:knight_slime_chestplate , armorplus:knight_slime_leggings , armorplus:knight_slime_boots

20. Chicken Armor: armorplus:chicken_helmet , armorplus:chicken_chestplate , armorplus:chicken_leggings , armorplus:chicken_boots

21. Slime Armor: armorplus:slime_helmet , armorplus:slime_chestplate , armorplus:slime_leggings , armorplus:slime_boots

22. Metal Armor: armorplus:metal_helmet , armorplus:metal_chestplate , armorplus:metal_leggings , armorplus:metal_boots

23. Electrical Armor: armorplus:electrical_helmet , armorplus:electrical_chestplate , armorplus:electrical_leggings , armorplus:electrical_boots

Items: armorplus:chainmail , armorplus:ender_dragon_scale , armorplus:reinforcing_material , armorplus:guardian_scale , armorplus:wither_bone , armorplus:the_ultimate_material , armorplus:lava_crystal , armorplus:the_gift_of_the_gods , armorplus:metal_ingot , armorplus:electrical_ingot

Blocks: armorplus:compressed_obsidian , armorplus:block_lava_crystal , armorplus:armor_forge , armorplus:metal_ore